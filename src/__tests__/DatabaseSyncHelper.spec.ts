import { expect } from 'chai';
import 'mocha';
import * as path from 'path';
import * as moment from 'moment';

import { FilesystemHelper } from '../filesystemHelper';
import { DatabaseSyncHelper } from '../databaseSyncHelper';

describe('DatabaseSyncHelper', () => {
  const workingAnonSalt = '$2b$05$8idDKDhxivv2JE1B6T9USe';
  describe('Data Anonymization', () => {
    it('should anonymize a string', () => {
      const dataString = 'MyDateWithout Anonymization';
      const anonData = DatabaseSyncHelper.anonymizeData(dataString);
      expect(anonData).to.be.not.equal(dataString);
      expect(typeof anonData).to.be.equal('string');
    });
  });

  describe('getSqlValueBasedOnType', () => {
    it('should return a text column as text', () => {
      const testString = 'MyExampleText';
      const tableColumn = { key: 'test', type: 'text' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, testString)).to.be.equal(`'${testString}'`);
    });
    it('should return a date column as a date', () => {
      const testDate = moment();
      const tableColumn = { key: 'test', type: 'date' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, testDate.toDate())).to.be.equal(
        `'${testDate.format('YYYY-MM-DD')}'`,
      );
    });
    it('should return a datetime column as datetime', () => {
      const testDate = moment();
      const tableColumn = { key: 'test', type: 'datetime' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, testDate.toDate())).to.be.equal(
        `'${testDate.format('YYYY-MM-DD HH:mm:ss')}'`,
      );
    });
    it('should return a raw column without any change', () => {
      const testString = '100';
      const tableColumn = { key: 'test', type: 'raw' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, 100)).to.be.equal(`${testString}`);
    });
    it('should return an unknown column without any change', () => {
      const testString = '100';
      const tableColumn = { key: 'test', type: 'unknown' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, 100)).to.be.equal(`${testString}`);
    });
    it('should map an empty field to NULL', () => {
      const tableColumn = { key: 'test', type: 'unknown' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, null)).to.be.equal(`NULL`);
    });
    it('should map a value if a map function is given', () => {
      const tableColumn = { key: 'test', type: 'text', mapFct: () => 'B' };
      expect(DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, 'A')).to.be.equal(`'B'`);
    });
  });
  describe('getValuesForInsert', () => {
    it('should concat fields if there are multiple', () => {
      const tableColumns = [
        { key: 'test', type: 'raw' },
        { key: 'test2', type: 'raw' },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = { test: 100, test3: 200 };
      expect(DatabaseSyncHelper.getValuesForInsert(tableColumns, dataRow)).to.be.equal(`100,NULL,200`);
    });
  });

  describe('getSqlCompareString', () => {
    it('should return a string with key and value comparison', () => {
      const testString = 'test = 100';
      const tableColumn = { key: 'test', type: 'raw' };
      expect(DatabaseSyncHelper.getSqlCompareString(tableColumn, 100)).to.be.equal(`${testString}`);
    });
  });

  describe('getPrimaryKeyFilter', () => {
    it('should return the condition if there is one primary key', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw' },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200, test3: 300 };
      expect(DatabaseSyncHelper.getPrimaryKeyFilterForOneValue(tableColumns, dataRow)).to.be.equal(`test = 100`);
    });
    it('should fail if there are multiple primary keys', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw', pk: true },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200, test3: 300 };
      expect(DatabaseSyncHelper.getPrimaryKeyFilterForOneValue.bind(null, tableColumns, dataRow)).to.throw(
        'Too many primary keys in table definition. Only one allowed.',
      );
    });
  });

  describe('getKeysForSelect', () => {
    it('should return all keys', () => {
      const tableColumns = [
        { key: 'test', type: 'raw' },
        { key: 'test2', type: 'raw' },
        { key: 'test3', type: 'raw' },
      ];
      expect(DatabaseSyncHelper.getKeysForSelect(tableColumns)).to.be.equal(`test,test2,test3`);
    });
    it('should return map keys of the field is given', () => {
      const tableColumns = [
        { key: 'test', type: 'raw' },
        { key: 'test2', type: 'raw' },
        { key: 'test3', sourceKey: 'MappedKey', type: 'raw' },
      ];
      expect(DatabaseSyncHelper.getKeysForSelect(tableColumns)).to.be.equal(`test,test2,MappedKey as test3`);
    });
  });

  describe('getKeysForInsert', () => {
    it('should return a key if there is only one', () => {
      const tableColumns = [{ key: 'test', type: 'raw' }];
      expect(DatabaseSyncHelper.getKeysForInsert(tableColumns)).to.be.equal(`test`);
    });
    it('should return all keys if there are multiple', () => {
      const tableColumns = [
        { key: 'test', type: 'raw' },
        { key: 'test2', type: 'raw' },
      ];
      expect(DatabaseSyncHelper.getKeysForInsert(tableColumns)).to.be.equal(`test,test2`);
    });
  });

  describe('getUpdateString', () => {
    it('should return all keys as compare string', () => {
      const tableColumns = [
        { key: 'test', type: 'raw' },
        { key: 'test2', type: 'raw' },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200, test3: 300 };
      expect(DatabaseSyncHelper.getUpdateString(tableColumns, dataRow)).to.be.equal(
        `test = 100,test2 = 200,test3 = 300`,
      );
    });
    it('should return all keys except the primary key as compare string', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw' },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200, test3: 300 };
      expect(DatabaseSyncHelper.getUpdateString(tableColumns, dataRow)).to.be.equal(`test2 = 200,test3 = 300`);
    });
  });

  describe('createUpdateSqlQuery', () => {
    it('should return the complete UPDATE string', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200 };
      expect(DatabaseSyncHelper.createUpdateSqlQuery('testTable', tableColumns, dataRow)).to.be.equal(
        `UPDATE testTable SET test2 = 200 WHERE test = 100;`,
      );
    });
  });

  describe('createInsertSqlQuery', () => {
    it('should return a complete insert string', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200 };
      expect(DatabaseSyncHelper.createInsertSqlQuery('testTable', tableColumns, dataRow)).to.be.equal(
        `INSERT INTO testTable (test,test2) SELECT 100,200 WHERE NOT EXISTS (SELECT 1 FROM testTable WHERE test = 100);`,
      );
    });
  });

  describe('createSelectQuery', () => {
    it('should return a complete select string', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw' },
      ];
      const dataRow = { test: 100, test2: 200 };
      expect(DatabaseSyncHelper.createSelectQuery('testTableSource', tableColumns)).to.be.equal(
        `SELECT test,test2 FROM testTableSource;`,
      );
    });
  });

  describe('createPostgresPoolConfig', () => {
    beforeEach(() => {
      process.env.PGUSER = 'PGUSER';
      process.env.PGHOST = 'PGHOST';
      process.env.PGDATABASE = 'PGDATABASE';
      process.env.PGPASSWORD = 'PGPASSWORD';
      process.env.PGPORT = '1234';
      process.env.PREF_PGUSER = 'PREFIX_PGUSER';
      process.env.PREF_PGHOST = 'PREFIX_PGHOST';
      process.env.PREF_PGDATABASE = 'PREFIX_PGDATABASE';
      process.env.PREF_PGPASSWORD = 'PREFIX_PGPASSWORD';
      process.env.PREF_PGPORT = '5678';
    });

    it('should return a config object if there is no prefix given', () => {
      expect(DatabaseSyncHelper.createPostgresPoolConfig()).to.deep.equal({
        user: 'PGUSER',
        host: 'PGHOST',
        database: 'PGDATABASE',
        password: 'PGPASSWORD',
        port: 1234,
      });
    });
    it('should return a config object if there is a prefix given', () => {
      expect(DatabaseSyncHelper.createPostgresPoolConfig('PREF')).to.deep.equal({
        user: 'PREFIX_PGUSER',
        host: 'PREFIX_PGHOST',
        database: 'PREFIX_PGDATABASE',
        password: 'PREFIX_PGPASSWORD',
        port: 5678,
      });
    });
  });

  describe('createPostgresPool & createDatabaseSyncer', () => {
    beforeEach(() => {
      process.env.PGUSER = 'postgres';
      process.env.PREF_PGUSER = 'postgres';
      process.env.PGHOST = 'localhost';
      process.env.PREF_PGHOST = 'localhost';
      process.env.PGDATABASE = 'testSyncSource';
      process.env.PREF_PGDATABASE = 'testSyncTarget';
      process.env.PGPASSWORD = 'postgres';
      process.env.PREF_PGPASSWORD = 'postgres';
      process.env.PGPORT = '5432';
      process.env.PREF_PGPORT = '5432';
    });

    it('should return a postgres pool', () => {
      expect(DatabaseSyncHelper.createPostgresPool()).to.be.a('object');
    });

    it('should return a database syncer', () => {
      const syncer = DatabaseSyncHelper.createDatabaseSyncer('', 'PREF', {
        sourceTableName: 'sourceTable',
        targetTableName: 'targetTableName',
        tableColumns: [],
      });
      expect(syncer).to.be.a('object');
    });
    it('should return a database syncer if no prefix is given', () => {
      const syncer = DatabaseSyncHelper.createDatabaseSyncer('PREF', undefined, {
        sourceTableName: 'sourceTable',
        targetTableName: 'targetTableName',
        tableColumns: [],
      });
      expect(syncer).to.be.a('object');
    });
  });

  describe('createDeleteSqlQuery', () => {
    it('should return a complete DELETE query', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw' },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = [
        { test: 100, test2: 200, test3: 300 },
        { test: 101, test2: 201, test3: 301 },
        { test: 102, test2: 202, test3: 302 },
      ];
      expect(DatabaseSyncHelper.createDeleteSqlQuery('testTable', tableColumns, dataRow)).to.be.equal(
        `DELETE FROM testTable WHERE test NOT IN (100,101,102);`,
      );
    });
    it('should fail if there are multiple primary keys', () => {
      const tableColumns = [
        { key: 'test', type: 'raw', pk: true },
        { key: 'test2', type: 'raw', pk: true },
        { key: 'test3', type: 'raw' },
      ];
      const dataRow = [
        { test: 100, test2: 200, test3: 300 },
        { test: 101, test2: 201, test3: 301 },
        { test: 102, test2: 202, test3: 302 },
      ];
      expect(DatabaseSyncHelper.createDeleteSqlQuery.bind(null, 'testTable', tableColumns, dataRow)).to.throw(
        'Too many primary keys in table definition. Only one allowed.',
      );
    });
  });
});
