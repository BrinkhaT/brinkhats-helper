import { DatabaseSyncHelper } from './databaseSyncHelper';
import { Pool } from 'pg';

export class DatabaseSyncer {
  private sourceDatabasePool: Pool;
  private targetDatabasePool: Pool;
  private syncConfig: SyncDatabaseTable;

  constructor(sourceEnvPrefix: string, targetEnvPrefix: string, syncTable: SyncDatabaseTable) {
    this.sourceDatabasePool = DatabaseSyncHelper.createPostgresPool(sourceEnvPrefix);
    this.targetDatabasePool = DatabaseSyncHelper.createPostgresPool(targetEnvPrefix);
    this.syncConfig = syncTable;
  }

  async sync() {
    const selectQuery = DatabaseSyncHelper.createSelectQuery(
      this.syncConfig.sourceTableName,
      this.syncConfig.tableColumns,
    );
    const sourceData = await this.getQueryResultFromSource(selectQuery);

    let i;
    let j;
    let temporary;
    const chunk = 1000;
    for (i = 0, j = sourceData.rows.length; i < j; i += chunk) {
      const allQueries: any[] = [];
      temporary = sourceData.rows.slice(i, i + chunk);
      // do whatever
      for (const dataRow of sourceData.rows) {
        allQueries.push(
          DatabaseSyncHelper.createInsertSqlQuery(
            this.syncConfig.targetTableName,
            this.syncConfig.tableColumns,
            dataRow,
          ),
        );
        allQueries.push(
          DatabaseSyncHelper.createUpdateSqlQuery(
            this.syncConfig.targetTableName,
            this.syncConfig.tableColumns,
            dataRow,
          ),
        );
      }
      await this.runQuerysAndCommitAgainstTarget(allQueries);
    }

    const deleteQuery = DatabaseSyncHelper.createDeleteSqlQuery(
      this.syncConfig.targetTableName,
      this.syncConfig.tableColumns,
      sourceData.rows,
    );
    await this.runQuerysAndCommitAgainstTarget([deleteQuery]);
  }

  async appendDataToSyncedTable(sourceTableName: string, tableColumns: SyncTableColumns[]) {
    const selectQuery = DatabaseSyncHelper.createSelectQuery(sourceTableName, tableColumns);
    const sourceData = await this.getQueryResultFromSource(selectQuery);
    const allQueries = [];

    for (const dataRow of sourceData.rows) {
      allQueries.push(DatabaseSyncHelper.createUpdateSqlQuery(this.syncConfig.targetTableName, tableColumns, dataRow));
    }
    await this.runQuerysAndCommitAgainstTarget(allQueries);
  }

  async close() {
    await this.sourceDatabasePool.end();
    await this.targetDatabasePool.end();
  }

  private getQueryResultFromSource(query: string) {
    return this.sourceDatabasePool.query(query);
  }

  private runQueryAgainstTarget(query: string) {
    return this.targetDatabasePool.query(query);
  }

  private async runQueryAndCommitAgainstTarget(query: string) {
    await this.runQueryAgainstTarget(query);
    return this.targetDatabasePool.query('COMMIT');
  }

  private async runQuerysAndCommitAgainstTarget(queries: string[]) {
    let i;
    let j;
    const chunk = 1000;
    for (i = 0, j = queries.length; i < j; i += chunk) {
      const temparray = queries.slice(i, i + chunk);
      await this.runQueryAndCommitAgainstTarget(temparray.join(''));
    }
  }
}

export interface SyncDatabaseTable {
  sourceTableName: string;
  targetTableName: string;
  tableColumns: SyncTableColumns[];
}

export interface SyncTableColumns {
  key: string;
  sourceKey?: string;
  type: string;
  anon?: boolean;
  pk?: boolean;
  mapNull?: boolean;
  mapFct?: (value: any) => any;
}
