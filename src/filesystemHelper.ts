import * as fs from 'fs';

export class FilesystemHelper {
  static GetListOfCsvFiles(folderPath: string) {
    return fs.readdirSync(folderPath).filter((fileName) => fileName.endsWith('.csv'));
  }
}
