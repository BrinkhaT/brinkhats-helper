import * as bcrypt from 'bcrypt';
import { Pool } from 'pg';
import * as moment from 'moment';
import { DatabaseSyncer, SyncDatabaseTable, SyncTableColumns } from './databaseSyncer';

export class DatabaseSyncHelper {
  static createDatabaseSyncer(sourceEnvPrefix: string, targetEnvPrefix: string = '', syncTable: SyncDatabaseTable) {
    return new DatabaseSyncer(sourceEnvPrefix, targetEnvPrefix, syncTable);
  }

  static createPostgresPool(envPrefix?: string) {
    return new Pool(DatabaseSyncHelper.createPostgresPoolConfig(envPrefix));
  }

  static createPostgresPoolConfig(envPrefix: string = '') {
    if (envPrefix !== '') envPrefix += '_';

    return {
      user: process.env[`${envPrefix}PGUSER`],
      host: process.env[`${envPrefix}PGHOST`],
      database: process.env[`${envPrefix}PGDATABASE`],
      password: process.env[`${envPrefix}PGPASSWORD`],
      port: Number(process.env[`${envPrefix}PGPORT`]),
    };
  }

  static createSelectQuery = (sourceTable: string, tableColumns: SyncTableColumns[]) => {
    return `SELECT ${DatabaseSyncHelper.getKeysForSelect(tableColumns)} FROM ${sourceTable};`;
  };

  static createInsertSqlQuery = (targetTableName: string, tableColumns: SyncTableColumns[], dataRow: any) => {
    return `INSERT INTO ${targetTableName} (${DatabaseSyncHelper.getKeysForInsert(
      tableColumns,
    )}) SELECT ${DatabaseSyncHelper.getValuesForInsert(
      tableColumns,
      dataRow,
    )} WHERE NOT EXISTS (SELECT 1 FROM ${targetTableName} WHERE ${DatabaseSyncHelper.getPrimaryKeyFilterForOneValue(
      tableColumns,
      dataRow,
    )});`;
  };

  static createUpdateSqlQuery = (targetTableName: string, tableColumns: SyncTableColumns[], dataRow: any) => {
    return `UPDATE ${targetTableName} SET ${DatabaseSyncHelper.getUpdateString(
      tableColumns,
      dataRow,
    )} WHERE ${DatabaseSyncHelper.getPrimaryKeyFilterForOneValue(tableColumns, dataRow)};`;
  };

  static createDeleteSqlQuery = (targetTableName: string, tableColumns: SyncTableColumns[], dataRows: any[]) => {
    return `DELETE FROM ${targetTableName} WHERE ${DatabaseSyncHelper.getPrimaryKeyFilterForMultipleValues(
      tableColumns,
      dataRows,
    )};`;
  };

  static getUpdateString = (tableColumns: SyncTableColumns[], dataRow: any) => {
    return tableColumns
      .filter((tableColumn) => !Boolean(tableColumn.pk))
      .map((tableColumn) => DatabaseSyncHelper.getSqlCompareString(tableColumn, dataRow[tableColumn.key]))
      .join(',');
  };

  static getKeysForInsert = (tableColumns: SyncTableColumns[]) => {
    return tableColumns.map((tableColumn) => tableColumn.key).join(',');
  };

  static getKeysForSelect = (tableColumns: SyncTableColumns[]) => {
    return tableColumns
      .map((tableColumn) => {
        if (tableColumn.sourceKey) return `${tableColumn.sourceKey} as ${tableColumn.key}`;
        return tableColumn.key;
      })
      .join(',');
  };

  static getPrimaryKeyColumn = (tableColumns: SyncTableColumns[]) => {
    const pkArray = tableColumns.filter((tableColumn) => Boolean(tableColumn.pk));

    if (pkArray.length !== 1) {
      throw new Error('Too many primary keys in table definition. Only one allowed.');
    }
    return pkArray[0];
  };

  static getPrimaryKeyFilterForMultipleValues = (tableColumns: SyncTableColumns[], dataRows: any[]) => {
    const primaryKeyColumn = DatabaseSyncHelper.getPrimaryKeyColumn(tableColumns);

    const filterCondition = dataRows
      .map((dataRow) => `${DatabaseSyncHelper.getSqlValueBasedOnType(primaryKeyColumn, dataRow[primaryKeyColumn.key])}`)
      .join(',');

    return `${primaryKeyColumn.key} NOT IN (${filterCondition})`;
  };

  static getPrimaryKeyFilterForOneValue = (tableColumns: SyncTableColumns[], row: any) => {
    const primaryKeyColumn = DatabaseSyncHelper.getPrimaryKeyColumn(tableColumns);
    return DatabaseSyncHelper.getSqlCompareString(primaryKeyColumn, row[primaryKeyColumn.key]);
  };

  static getSqlCompareString = (tableColumn: SyncTableColumns, value: any) => {
    return `${tableColumn.key} = ${DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, value)}`;
  };

  static getValuesForInsert = (tableColumns: SyncTableColumns[], row: any) => {
    return tableColumns
      .map((tableColumn) => DatabaseSyncHelper.getSqlValueBasedOnType(tableColumn, row[tableColumn.key]))
      .join(',');
  };

  static getSqlValueBasedOnType = (tableColumn: SyncTableColumns, value: any) => {
    if (tableColumn.mapFct) value = tableColumn.mapFct(value);
    if (!tableColumn.mapNull && (value === null || value === undefined)) return `NULL`;
    if (tableColumn.anon) value = DatabaseSyncHelper.anonymizeData(value);
    switch (tableColumn.type) {
      case 'text':
        return `'${value.replace("'", "''")}'`;
      case 'date':
        return `'${moment(value).format('YYYY-MM-DD')}'`;
      case 'datetime':
        return `'${moment(value).format('YYYY-MM-DD HH:mm:ss')}'`;
      case 'raw':
      default:
        return `${value}`;
    }
  };

  static anonymizeData(_data: string) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    const length = 10;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
