import { expect } from 'chai';
import 'mocha';

import { BankingHelper } from '../index';

describe('FillAccountNumberWithCheckDigit', () => {
  const failure = 'Account number has wrong length. Expected 7 digits';

  it('should return the correct number', () => {
    expect(BankingHelper.FillAccountNumberWithCheckDigit(5100000)).to.be.equal(5100000008);
  });
  it('should return the correct number (special case: check digit 0)', () => {
    expect(BankingHelper.FillAccountNumberWithCheckDigit(5100099)).to.be.equal(5100099000);
  });
  it('should fail if the number is to short', () => {
    expect(BankingHelper.FillAccountNumberWithCheckDigit.bind(null, 510000)).to.throw(failure);
  });
  it('should fail if the number is to long', () => {
    expect(BankingHelper.FillAccountNumberWithCheckDigit.bind(null, 5100000999)).to.throw(failure);
  });
});
