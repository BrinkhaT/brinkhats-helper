export class BankingHelper {
  /**
   * Calculates the check digit of a given account number based on an
   * official calculation method (*Modulus 10, Gewichtung 2, 1, 2, 1, 2, 1, 2, 1, 2*)
   *
   * @param accountNumber The account number without check digit (7 digits expected)
   * @returns The account number filled up to 10 digits with check digit at the end
   */
  static FillAccountNumberWithCheckDigit = (accountNumber: number) => {
    if (accountNumber.toString().length !== 7) {
      throw new Error('Account number has wrong length. Expected 7 digits');
    }

    // split given account number into the numbers as array
    const splitToNumbers = accountNumber
      .toString()
      .split('')
      .map((v) => Number(v));

    // multiply digits with sequence 2,1,2,1,2,1,2
    const multipliedWithTwoOne = splitToNumbers.map((v, i) => {
      if (i % 2 === 0) {
        return v * 2;
      }
      return v;
    });

    // calculate crossfoot of each multiple
    const squareNumbers = multipliedWithTwoOne.map((v) => {
      if (v.toString().length > 1) {
        return Number(v.toString().substring(0, 1)) + Number(v.toString().substring(1, 2));
      }
      return v;
    });

    // sum all numbers
    const sumOfNumbers = squareNumbers.reduce((p, v) => p + v, 0);

    // for next step, only the last digit is relevant
    const onePart = sumOfNumbers % 10;

    // subtract the number from 10
    let checkDigit = 10 - onePart;

    // is the result is 10, use 0 as check digit
    if (checkDigit === 10) checkDigit = 0;

    return Number(`${accountNumber}00${checkDigit}`);
  };
}
