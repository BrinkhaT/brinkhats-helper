import { expect } from 'chai';
import 'mocha';
import * as path from 'path';

import { FilesystemHelper } from '../filesystemHelper';

describe('FilesystemHelper', () => {
  it('should return CSVs from a folder', () => {
    const csvFiles = FilesystemHelper.GetListOfCsvFiles(path.join(__dirname, 'data/csvs'));
    expect(csvFiles.length).to.be.equal(2);
    expect(csvFiles).to.contain('fileOne.csv');
  });
});
